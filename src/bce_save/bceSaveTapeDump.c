/*
 Copyright 2013-2016 by Charles Anthony

 All rights reserved.

 This software is made available under the terms of the
 ICU License -- ICU 1.8.1 and later.
 See the LICENSE file at the top-level directory of this distribution and
 at https://sourceforge.net/p/dps8m/code/ci/master/tree/LICENSE
 */

/*
tape image format:

<32 bit little-endian blksiz> <data> <32bit little-endian blksiz>

a single 32 bit word of zero represents a file mark
*/

// From sim_tape.h
#define MTR_TMK         0x00000000                      /* tape mark */
#define MTR_EOM         0xFFFFFFFF                      /* end of medium */
#define MTR_GAP         0xFFFFFFFE                      /* primary gap */
#define MTR_RRGAP       0xFFFFFFFF                      /* reverse read half gap */
#define MTR_FHGAP       0xFFFEFFFF                      /* fwd half gap (overwrite) */
#define MTR_RHGAP       0xFFFF0000                      /* rev half gap (overwrite) */
#define MTR_M_RHGAP     (~0x000080FF)                   /* range mask for rev gap */
#define MTR_MAXLEN      0x00FFFFFF                      /* max len is 24b */
#define MTR_ERF         0x80000000                      /* error flag */
#define MTR_F(x)        ((x) & MTR_ERF)                 /* record error flg */
#define MTR_L(x)        ((x) & ~MTR_ERF)                /* record length */


// dcl       1 rec_header        aligned based,
//             2 c1              bit (36),                     /* Header pattern-1 */            word 0
//             2 type            fixed bin (17) unal,          /* record type */                 word 1
//             2 flags           unal,
//               3 end_of_set    bit (1),                      /* valid in TAPE_EOR records */
//               3 end_of_part   bit (1),                      /* last PV_PART record */
//               3 end_of_pv     bit (1),                      /* last PV record */
//               3 pad           bit (15),
//             2 rec_on_tape     fixed bin (35),               /* physical tape rec# */          word 2
//             2 pvid            bit (36),                     /* origin of data */              word 3
//             2 rec_on_pv       fixed bin (35),               /* volume rec# */                 word 4
//             2 rec_in_type     fixed bin,                    /* rec# of current rec type */    word 5
//             2 part_name       char (4),                     /* name of partition */           word 6
//                                                             /* when type = PV_PART */
//             2 tape_set_uid    bit (36);                     /* unique Tape SET ID */          word 7

// dcl       C1_PATTERN          bit (36) int static options (constant) init ("542553413076"b3);

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <ctype.h>
#include <inttypes.h>

// extract bits into a number
#include <stdint.h>

#include "bit36.h"


// dcl       TAPE_LABEL                                        /* tape label record */
//                               fixed bin static options (constant) init (1);
// dcl       TAPE_EOR                                          /* end-of-reel record */
//                               fixed bin static options (constant) init (2);
// dcl       PV_PREAMBLE                                       /* volume preamble */
//                               fixed bin static options (constant) init (3);
// dcl       PV_VTOC                                           /* disk records 0 - end of vtoc */
//                               fixed bin static options (constant) init (4);
// dcl       PV_RECORD                                         /* disk record */
//                               fixed bin static options (constant) init (5);
// dcl       PV_PART                                           /* disk partition */
//                               fixed bin static options (constant) init (6);

static char * recTypeStr (uint64_t t) {
  if (t == 1) return "TAPE_LABEL";
  if (t == 2) return "TAPE_EOR";
  if (t == 3) return "PV_PREAMBLE";
  if (t == 4) return "PV_VTOC";
  if (t == 5) return "PV_RECORD";
  if (t == 6) return "PV_PART";
  return "???";
}

int main (int argc, char * argv [])
  {
    int fd;
    uint32_t blksiz;
    int32_t blksiz2;
    //fd = open ("20184.tap", O_RDONLY);
    fd = open (argv [1], O_RDONLY);
    if (fd < 0)
      {
        printf ("can't open tape\n");
        exit (1);
      }
    int blkno = 0;
    for (;;)
      {
        ssize_t sz;
        off_t pos = lseek (fd, 0, SEEK_CUR);
        sz = read (fd, & blksiz, sizeof (blksiz));
        if (sz == 0)
          break;
        if (sz != sizeof (blksiz))
          {
            printf ("can't read blksiz\n");
            exit (1);
          }
        printf ("blksiz %u pos %ld\n", blksiz, pos);

        //if (! blksiz)
        if (blksiz == MTR_TMK)
          {
            printf ("tapemark\n");
          }
        else if (blksiz == MTR_EOM)
          {
            printf ("end-of-medium\n");
            break;
          }
        else if (blksiz == MTR_GAP)
          {
            printf ("primary gap");
          }
        else if (blksiz == MTR_RRGAP)
          {
            printf ("reverse read half gap");
          }
        else if (blksiz == MTR_FHGAP)
          {
            printf ("fwd half gap");
          }
        else if (blksiz == MTR_RHGAP)
          {
            printf ("rev half gap");
          }
        else if (blksiz > MTR_MAXLEN)
          {
            printf ("unknown metadata type 0x%x\n", blksiz);
          }
        else
          {
// 72 bits at a time; 2 dps8m words == 9 bytes
            int i;
            uint8_t bytes [9];
            int wordno = 0;
            for (i = 0; i < blksiz; i += 9)
              {
                int n = 9;
                if (i + 9 > blksiz)
                  n = blksiz - i;
                memset (bytes, 0, 9);
                sz = read (fd, bytes, n);
//{ int jj; for (jj = 0; jj < 72; jj ++) { uint64_t k = extr (bytes, jj, 1); printf ("%ld", k); } printf ("\n"); }
//printf ("%02X %02X %02X %02X %02X\n", bytes [0], bytes [1], bytes [2], bytes [3], bytes [4]);
                if (sz == 0)
                  {
                    printf ("eof whilst skipping byte\n");
                    exit (1);
                  }
                if (sz != n)
                  {
                    printf ("can't skip bytes\n");
                    exit (1);
                  }
                uint64_t w0 = extr (bytes, 0, 36);
//printf ("%012"PRIo64"\n", w0);
                uint64_t w1 = extr (bytes, 36, 36);

                if (blksiz == 36) {
                  if (wordno == 0) {
                    // c1
                    if (w0 != 0542553413076) {
                      printf ("c1 wrong; expected %012llo, found %012ll0\n", 0542553413076, w0);
                    }
                    uint64_t recordType = w1 >> 18;
                    printf ("   record type %d %s\n", recordType, recTypeStr (recordType));
                    printf ("     end_of_set  %o\n", (w0 & 0400000) ? 1 : 0);
                    printf ("     end_of_part %o\n", (w0 & 0200000) ? 1 : 0);
                    printf ("     end_of_pv   %o\n", (w0 & 0100000) ? 1 : 0);
                  } else if (wordno == 2) {
                    printf ("   rec_on_tape   %012llo\n", w0);
                    printf ("   pvid          %012llo\n", w1);
                  } else if (wordno == 4) {
                    printf ("   rec_on_pv    %lld\n", w0);
                    printf ("   rec_in_type  %lld\n", w1);
                  } else if (wordno == 6) {
                    unsigned int c0 = (w0 >> (3 * 9)) & 0177;
                    unsigned int c1 = (w0 >> (2 * 9)) & 0177;
                    unsigned int c2 = (w0 >> (1 * 9)) & 0177;
                    unsigned int c3 = (w0 >> (0 * 9)) & 0177;
                    printf ("   part_name  %c%c%c%c\n",
                      isprint (c0) ? c0 : '?',
                      isprint (c1) ? c1 : '?',
                      isprint (c2) ? c2 : '?',
                      isprint (c3) ? c3 : '?');
                    printf ("   tape_set_uid %llo\n", w1);
                  }
                } else {
                  if (wordno == 0)
                    printf ("    data block\n");
                }
                wordno += 2;
#if 0
                // int text [8]; // 8 9-bit bytes in 2 words
                printf ("%04d:%04o   %012"PRIo64"   %012"PRIo64"   \"", blkno, i * 2 / 9, w0, w1);
                int j;
//{printf ("\n<<%012"PRIo64">>\n", (* (uint64_t *) bytes) & 0777777777777); }

                static int byteorder [8] = { 3, 2, 1, 0, 7, 6, 5, 4 };
                for (j = 0; j < 8; j ++)
                  {
                    uint64_t c = extr (bytes, byteorder [j] * 9, 9);
                    if (isprint (c))
                      printf ("%c", (char) c);
                    else
                      printf ("\\%03lo", c);
                  }
                printf ("\n");
#endif
              }
            // blksiz words are on 16 bit boundaries.
            if (blksiz % 2)
              {
                 lseek (fd, 1, SEEK_CUR);
              }
            sz = read (fd, & blksiz2, sizeof (blksiz2));
            if (sz != sizeof (blksiz2))
              {
                printf ("can't read blksiz2\n");
                exit (1);
              }
            if (blksiz != blksiz2)
              {
                printf ("blksiz != blksiz2\n");
                exit (1);
              }
            printf ("\n");
            blkno ++;
          }
      }
    return 0;
  }
