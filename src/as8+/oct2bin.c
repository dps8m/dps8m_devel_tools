#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>

typedef uint64_t    word36;

static void put36 (word36 val, uint8_t * bits, uint woffset)
  {
    uint isOdd = woffset % 2;
    uint dwoffset = woffset / 2;
    uint8_t * p = bits + dwoffset * 9;

    if (isOdd)
      {
        p [4] &=               0xf0;
        p [4] |= (val >> 32) & 0x0f;
        p [5]  = (val >> 24) & 0xff;
        p [6]  = (val >> 16) & 0xff;
        p [7]  = (val >>  8) & 0xff;
        p [8]  = (val >>  0) & 0xff;
        //w  = ((uint64) (p [4] & 0xf)) << 32;
        //w |=  (uint64) (p [5]) << 24;
        //w |=  (uint64) (p [6]) << 16;
        //w |=  (uint64) (p [7]) << 8;
        //w |=  (uint64) (p [8]);
      }
    else
      {
        p [0]  = (val >> 28) & 0xff;
        p [1]  = (val >> 20) & 0xff;
        p [2]  = (val >> 12) & 0xff;
        p [3]  = (val >>  4) & 0xff;
        p [4] &=               0x0f;
        p [4] |= (val <<  4) & 0xf0;
        //w  =  (uint64) (p [0]) << 28;
        //w |=  (uint64) (p [1]) << 20;
        //w |=  (uint64) (p [2]) << 12;
        //w |=  (uint64) (p [3]) << 4;
        //w |= ((uint64) (p [4]) >> 4) & 0xf;
      }
    // mask shouldn't be neccessary but is robust
  }
int main (int argc, char * argv [])
  {
    char buf[256];
    uint8_t * segment;       
    size_t sz;
    for (;;)
      {
        if (! fgets (buf, sizeof (buf),  stdin))
          break;
        unsigned int size;
        if (sscanf (buf, "!SIZE %o", & size) == 1)
          {
            sz = size * 36 / 8;
            segment = malloc (sz);
            memset (segment, 0, sz);
            continue;
          }
        unsigned int addr;
        word36 v;
        if (sscanf (buf, "%o xxxx %llo", & addr, & v) == 2)
          {
            put36 (v, segment, addr);
            continue;
          }
      }
    write (fileno (stdout), segment, sz);
  }
